/*--------------------------------------------------------------------
 * Copyright 2018-2019 Yani M. All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * 07/2018, Mexico City, MEX
 * 07/2019, El Paso, TX, USA
 *--------------------------------------------------------------------
*/

/* @file mainlrcwindow.h
 * @brief Contains method definition of the LRC Generator application.
 *
 * This is a LRC Generator application developed in Qt intended to add time labels to a .txt file
 * which contains a lyric song. This will be improved and released gradually.
 *
 * @author YAN
*/

#ifndef MAINLRCWINDOW_H
#define MAINLRCWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QMediaPlayer>
#include <QMediaPlaylist>


#include <QLabel>
#include <QSlider>
#include <QLabel>
#include <QNetworkReply>


namespace Ui {
class MainLRCWindow;
}

class MainLRCWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainLRCWindow(QWidget *parent = 0);
    ~MainLRCWindow();
    int CalcArbitraryTimes();
    void addToPlaylist(const QList<QUrl> &urls);
    int volume() const;
    bool isMuted() const;

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_btnOpenAudio_clicked();

    void on_btnPlay_clicked();

    void on_sliSong_sliderMoved(int position);

    /// slots for MediaPlayer object mp_player
    void durationChanged(qint64 duration);
    void positionChanged(qint64 progress);

    void on_btnStop_clicked();

    void on_listView_doubleClicked(const QModelIndex &index);

    /// slot to update labels with the current, previos and next line
    void updateLabels();

    void on_btnSave_clicked();

    void on_sliVolume_valueChanged(int value);

    void setVolume(int volume);

    void on_sliSong_valueChanged(int value);

    void on_sliVolume_sliderMoved(int position);

    void on_btnMute_clicked();

    void on_pushButton_3_clicked();

    void on_btbSearch_clicked();

    void replyFinishedLyric(QNetworkReply *qnr);

signals:
    void play();
    void pause();
    void stop();
    void next();
    void previous();
    void changeVolume(int volume);
    void changeMuting(bool muting);
    void changeRate(qreal rate);

private:

    void updateDurationInfo(qint64 currentInfo);

    Ui::MainLRCWindow *ui;

    QString currentFile;

    QMediaPlayer::State m_playerState = QMediaPlayer::StoppedState;

    QMediaPlayer *m_player = nullptr;
    QMediaPlaylist *m_playlist = nullptr;

    bool m_playerMuted = false;
    /*
    QLabel *m_coverLabel = nullptr;
    QSlider *m_slider = nullptr;
    QLabel *m_labelDuration = nullptr;

    QString m_trackInfo;
    QString m_statusInfo;
    */
    qint64 m_duration;
    QString tStr;

};

#endif // MAINLRCWINDOW_H
