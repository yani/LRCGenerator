/*--------------------------------------------------------------------
 * Copyright 2018-2019 Yani M. All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * 07/2018, Mexico City, MEX
 * 07/2019, El Paso, TX, USA
 *--------------------------------------------------------------------
*/

/* @file mainlrcwindow.cpp
 * @brief Contains some business logic of the LRC Generator application.
 *
 * This is a LRC Generator application developed in Qt intended to add time labels to a .txt file
 * which contains a lyric song. This will be improved and released gradually.
 *
 * @author YAN
*/

#include "mainlrcwindow.h"
#include "ui_mainlrcwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QTextEdit>
#include <iostream>
#include <QTime>
#include <QTimer>
#include <QStandardPaths>
#include <QStringListModel>
#include <QStandardItem>
#include <QString>
#include <QColorDialog>
#include <QAudio>
#include <QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QUrl>

using namespace std;
MainLRCWindow::MainLRCWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainLRCWindow)
{
    /// shows the wiundow
    ui->setupUi(this);
    MainLRCWindow::showMaximized();
    //MainLRCWindow::showFullScreen();
    ///Go to fullscreen, alternative
    //QTimer::singleShot(0, this, SLOT(showFullScreen()));

    /// set icons for media buttons:
    ui->btnPlay->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    ui->btnStop->setIcon(style()->standardIcon(QStyle::SP_MediaStop));
    ui->btnMute->setIcon(style()->standardIcon(QStyle::SP_MediaVolume));

    /// set the color for the 3 labels: previous, current and next lines:
    QPalette palette = ui->lblPrevLine->palette();
    palette.setColor(QPalette::WindowText, Qt::gray);
    ui->lblPrevLine->setPalette(palette);

    palette = ui->lblCurrLine->palette();
    palette.setColor(QPalette::WindowText, Qt::blue);
    ui->lblCurrLine->setPalette(palette);

    palette = ui->lblNextLine->palette();
    palette.setColor(QPalette::WindowText, Qt::gray);
    ui->lblNextLine->setPalette(palette);

    palette = ui->lblNextLine2->palette();
    palette.setColor(QPalette::WindowText, Qt::gray);
    ui->lblNextLine2->setPalette(palette);

    /// create the multimedia object:
    m_player = new QMediaPlayer(this);
    m_player->setAudioRole(QAudio::VideoRole);
    qInfo() << "Supported audio roles:";
    for (QAudio::Role role : m_player->supportedAudioRoles())
        qInfo() << "    " << role;

    // owned by PlaylistModel
    m_playlist = new QMediaPlaylist();
    m_player->setPlaylist(m_playlist);

    /// create links between signals and slots for MediaPlayer object:
    connect(m_player, &QMediaPlayer::durationChanged, this, &MainLRCWindow::durationChanged);
    connect(m_player, &QMediaPlayer::positionChanged, this, &MainLRCWindow::positionChanged);

    /// connect the signal from volume slider with the setVolume methos in QMediaPlayer
    connect(this, &MainLRCWindow::changeVolume, m_player, &QMediaPlayer::setVolume);
    //connect(m_player, &PlayerControls::changeMuting, m_player, &QMediaPlayer::setMuted);

    /// sets the range for the song slider
    ui->sliSong->setRange(0, m_player->duration() / 1000);

    /// sets the range for the volume slider
    ui->sliVolume->setRange(0, 100);
    setVolume(m_player->volume());
}

MainLRCWindow::~MainLRCWindow()
{
    delete ui;
}

void MainLRCWindow::on_pushButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select the lyric file");
    QFile file(fileName);
    currentFile = fileName;
    if(!file.open(QIODevice::ReadOnly | QFile::Text)){
        QMessageBox::warning(this, "..","File not opened");
        return;
    }
    QTextStream in(&file);
    QString text = in.readAll();        

    ///populates the QListView object
    QStringListModel *model = new QStringListModel(this);          ;
    QStringList strList = text.split(QRegExp("\n"), QString::SkipEmptyParts);
    model->setStringList(strList);
    ui->listView->setModel(model);

    file.close();

}

void MainLRCWindow::on_pushButton_2_clicked()
{

    //QMessageBox::warning(this, "..", "DBG: "+ui->textEdit->get)
    CalcArbitraryTimes();
}

int MainLRCWindow::CalcArbitraryTimes(){
    /// old way using textEdit:
    ///QString data = ui->textEdit->toPlainText();
    ///QStringList strList = data.split(QRegExp("\n"), QString::SkipEmptyParts);

    /// get the value from the QListView and stores in an QStringList:
    QAbstractItemModel* modelaux = ui->listView->model();
    QStringList strList;
    for ( int i = 0 ; i < modelaux->rowCount() ; ++i )
    {
      // Get item at row i, col 0.
      strList << modelaux->index( i, 0 ).data( Qt::DisplayRole ).toString();
    }

    QTime timeSong(0,0,0);

    int size = strList.size();  ///< number of lines of the song
    for(int i=0;i<size;i++){
        strList.replace(i, "[" + QString::number(timeSong.minute()) + ":" +  QString::number(timeSong.second()) + "." +  QString::number(timeSong.msec(), 10) + "]" + strList[i]);
        timeSong = timeSong.addSecs(4);
    }

    /// populates the QListView object
    QStringListModel *model = new QStringListModel(this);          ;
    model->setStringList(strList);

    ui->listView->setModel(model);
    ui->listView->show();

    /// old way using textEdit:
    //ui->textEdit->setText(strList.join("\n"));


    return 0;
}

/*!
 * \brief Static method to check for .m3u playlists
 *
 */

static bool isPlaylist(const QUrl &url) // Check for ".m3u" playlists.
{
    if (!url.isLocalFile())
        return false;
    const QFileInfo fileInfo(url.toLocalFile());
    return fileInfo.exists() && !fileInfo.suffix().compare(QLatin1String("m3u"), Qt::CaseInsensitive);
}

void MainLRCWindow::addToPlaylist(const QList<QUrl> &urls)
{
    for (auto &url: urls) {
        if (isPlaylist(url))
            m_playlist->load(url);
        else
            m_playlist->addMedia(url);
    }
}

int MainLRCWindow::volume() const
{
    qreal linearVolume = QAudio::convertVolume(ui->sliVolume->value() / qreal(100),
                                               QAudio::LogarithmicVolumeScale,
                                               QAudio::LinearVolumeScale);
    return qRound(linearVolume * 100);
}

bool MainLRCWindow::isMuted() const
{
    return m_playerMuted;
}

void MainLRCWindow::on_btnOpenAudio_clicked()
{
    QFileDialog fileDialog(this);
    fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
    fileDialog.setWindowTitle(tr("Open Files"));
    QStringList supportedMimeTypes = m_player->supportedMimeTypes();
    if (!supportedMimeTypes.isEmpty()) {
        supportedMimeTypes.append("audio/x-m3u"); // MP3 playlists
        fileDialog.setMimeTypeFilters(supportedMimeTypes);
    }
    fileDialog.setDirectory(QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).value(0, QDir::homePath()));
    if (fileDialog.exec() == QDialog::Accepted)
        addToPlaylist(fileDialog.selectedUrls());
}


void MainLRCWindow::on_btnPlay_clicked()
{
    switch (m_playerState) {
    case QMediaPlayer::StoppedState:
        ui->btnStop->setEnabled(true);
        ui->btnPlay->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
        m_playerState = QMediaPlayer::PlayingState;
        m_player->play();
        break;

    case QMediaPlayer::PausedState:
        // emit play();
        m_player->play();
        ui->btnStop->setEnabled(true);
        m_playerState = QMediaPlayer::PlayingState;
        ui->btnPlay->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
        break;
    case QMediaPlayer::PlayingState:
        //emit pause();
        m_player->pause();
        ui->btnStop->setEnabled(true);
        m_playerState = QMediaPlayer::PausedState;
        ui->btnPlay->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
        break;
    }
}

void MainLRCWindow::updateDurationInfo(qint64 currentInfo)
{
    ///QString tStr;
    if (currentInfo || m_duration) {
        QTime currentTime((currentInfo / 3600) % 60, (currentInfo / 60) % 60,
            currentInfo % 60, (currentInfo * 1000) % 1000);
        QTime totalTime((m_duration / 3600) % 60, (m_duration / 60) % 60,
            m_duration % 60, (m_duration * 1000) % 1000);
        QString format = "mm:ss";
        if (m_duration > 3600)
            format = "hh:mm:ss";
        tStr = currentTime.toString(format);

        ui->lblCurrentTime->setText(tStr + " / " + totalTime.toString(format));
    }

    //PENDING
    //cout<<"modelcolumn "<<ui->listView->model()-><<".end";

    updateLabels();
}

void MainLRCWindow::on_sliSong_sliderMoved(int position)
{
    /// assume that position are seconds
    m_player->setPosition(position * 1000);
}

void MainLRCWindow::durationChanged(qint64 duration)
{
    m_duration = duration / 1000;
    ui->sliSong->setMaximum(m_duration);
}

void MainLRCWindow::positionChanged(qint64 progress)
{
    if (!ui->sliSong->isSliderDown())
        ui->sliSong->setValue(progress / 1000);

    updateDurationInfo(progress / 1000);
}

void MainLRCWindow::on_btnStop_clicked()
{
    m_player->stop();
    m_playerState = QMediaPlayer::StoppedState;
    ui->btnPlay->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    ui->btnStop->setEnabled(false);


}

void MainLRCWindow::on_listView_doubleClicked(const QModelIndex &index)
{
    ///To read a value from a QListView widget:
    ///ui->listView->model()->index(index.row(),0).data(Qt::DisplayRole).toString();

    QVariant qvariant;
    QString qstr = ui->listView->model()->index(index.row(),0).data(Qt::DisplayRole).toString();

    /// format the current time
    QString qstr2 = ui->lblCurrentTime->text();
    QStringList qstrlis = ui->lblCurrentTime->text().split("/");
    qstr2 = qstrlis[0].replace(" ","");
    qstr.replace(QRegularExpression("\\[.*\\]"), "["+ qstr2 +".0"+"]");
    qvariant = QString(qstr);

    ///To set a value in a QListView widget:
    ui->listView->selectionModel()->model()->setData(index, qvariant, Qt::EditRole);

    //QStandardItem *item = new QStandardItem(QString("item %0").arg(i));
    //QStandardItem *item = ui->listView->model()->index(index.row(),0).data(Qt::DisplayRole).toString();
}

/*!
 *
 *
 */
void MainLRCWindow::updateLabels()
{    
    QString qstr;
    string str;
    for(int i=0;i<ui->listView->model()->rowCount();i++){

        /// set the content for 3 labels: current, previous and next lines
        qstr = ui->listView->model()->index(i,0).data(Qt::DisplayRole).toString();
        size_t pos=qstr.toStdString().find(tStr.toStdString());

        if ( pos != string::npos){
            /// set current line
            unsigned long long size = tStr.toStdString().size();                        
            ui->lblCurrLine->setText(qstr.mid(pos+size+3, string::npos));


            /// sets the lyrics for previous and next labels
            if(i>0) ///< previous
                ui->lblPrevLine->setText(ui->listView->model()->index(i-1,0).data(Qt::DisplayRole).toString().mid(pos+size+3, string::npos));

            if(i < ui->listView->model()->rowCount()){

                ui->lblNextLine->setText(ui->listView->model()->index(i+1,0).data(Qt::DisplayRole).toString().mid(pos+size+3, string::npos));
                ui->lblNextLine2->setText(ui->listView->model()->index(i+2,0).data(Qt::DisplayRole).toString().mid(pos+size+3, string::npos));

            }
        }
        str.substr();
        cout<<qstr.toStdString()<<endl;
    }




    //ui->lblCurrLine->setText(ui->listView->model()->index(index.row(),0).data(Qt::DisplayRole).toString());
}

void MainLRCWindow::on_btnSave_clicked()
{

    QString fileName = QFileDialog::getSaveFileName(this, "Specify the name to save your .lrc file");
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);
    for ( int i = 0 ; i < ui->listView->model()->rowCount() ; ++i )
    {
      // Get item at row i, col 0.
      out<<ui->listView->model()->index(i, 0 ).data( Qt::DisplayRole).toString();
      out<<endl;
    }
    file.close();

}

void MainLRCWindow::on_sliVolume_valueChanged(int value)
{
    emit changeVolume(volume());
}

void MainLRCWindow::setVolume(int volume)
{
    qreal logarithmicVolume = QAudio::convertVolume(volume / qreal(100),
                                                    QAudio::LinearVolumeScale,
                                                    QAudio::LogarithmicVolumeScale);

    ui->sliVolume->setValue(qRound(logarithmicVolume * 100));
} 

void MainLRCWindow::on_btnMute_clicked()
{
    if (!m_playerMuted) {
        m_playerMuted = true;

        ui->btnMute->setIcon(style()->standardIcon(m_playerMuted
                ? QStyle::SP_MediaVolumeMuted
                : QStyle::SP_MediaVolume));
        m_player->setMuted(true);
    } else {
        m_playerMuted = false;

        ui->btnMute->setIcon(style()->standardIcon(m_playerMuted
                ? QStyle::SP_MediaVolumeMuted
                : QStyle::SP_MediaVolume));
        m_player->setMuted(false);

    }
}

void debugSocketVersion(){
    qDebug() <<"DBG: "<< QSslSocket::supportsSsl() << QSslSocket::sslLibraryBuildVersionString() << "version: " <<QSslSocket::sslLibraryVersionString();
}

/*
void MainLRCWindow::on_btbSearch_clicked()
{

    debugSocketVersion();
    QNetworkAccessManager mgr;
    //QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
    ///matcher.lyrics.get?APPID=87c1c739acb5fb84c205655d54107b14&q_track=heart%20of%20glass&q_artist=blondie

    //QNetworkRequest req( QUrl( QString("http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=7c04dcee7a8725669f7f49f4cc6f7394") ) );
    //QNetworkRequest req( QUrl( QString("http://matcher.lyrics.get?apikey=87c1c739acb5fb84c205655d54107b14&q_track=heart%20of%20glass&q_artist=blondie") ) );
    QNetworkRequest req;
    QUrl qurl;
    QUrlQuery qurlq;
    qurlq.addQueryItem("format","jsonp");
    qurlq.addQueryItem("callback","callback");
    qurlq.addQueryItem("q_track","heart%20of%20glass");
    qurlq.addQueryItem("q_artist","blondie");
    qurlq.addQueryItem("apikey","87c1c739acb5fb84c205655d54107b14");    ///<Change this

    qurl.setScheme("https");
    qurl.setHost("api.musixmatch.com");
    //qurl.("/ws/1.1");
    qurl.setPath("/ws/1.1/matcher.lyrics.get");
    qurl.setQuery(qurlq);
    //qurl = qurl.toEncoded();

    req.setUrl(qurl);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    connect(&mgr,SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinishedLyric(QNetworkReply*)));

    qDebug() <<"qurl: "<< qurl.toEncoded();

    QNetworkReply *reply = mgr.get(req);

    /// since QNetworkAccessManager does not offer synchronous HTTP requests, we use an event loop to emulate it:
    QEventLoop eventLoop;
    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {



     // QString strReply = (QString)reply->readAll();
     // QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
     // QJsonObject jsonObj = jsonResponse.object();

     // qDebug() << "name: " << jsonObj["name"].toString();
     // qDebug() << "longitude: " << jsonObj.value(QString("coord")).toObject()["lon"].toDouble();

        qDebug() << "Reply: " << reply;
        qDebug() << "Reply: " << reply->readAll();
        cout<< "Reply: " << reply->readAll().toStdString();
        delete reply;

    }else{
        qDebug() << "Failure" <<reply->errorString();
        delete reply;
    }

    /// to avoid a memory leak on each click (hint from stackoverflow):
    connect(&mgr,&QNetworkAccessManager::finished, &mgr, &QNetworkAccessManager::deleteLater);

}
*/

void MainLRCWindow::on_btbSearch_clicked()
{

    debugSocketVersion();
    QNetworkAccessManager mgr;
    //QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
    ///matcher.lyrics.get?APPID=87c1c739acb5fb84c205655d54107b14&q_track=heart%20of%20glass&q_artist=blondie

    QNetworkRequest req;
    QUrl qurl;
    QUrlQuery qurlq;
    //qurlq.addQueryItem("format","jsonp");
    //qurlq.addQueryItem("callback","callback");

    //qurlq.addQueryItem(NULL,"blondie/heart%20of%20glass");



    //qurlq.addQueryItem("q_artist","blondie");

    qurlq.addQueryItem("apikey","GxDMrBTSZJXDiJnTkJPCvVLZyvXQs83ghVCLAklLuH0NFleKNeEbdBPBGZvxigqZ");    ///<Change this

    /// temporary way:
    ///
    qurlq.setQueryDelimiters('=','?');
    //qurlq.setQuery(qurlq.query() + "blondie/heart%20of%20glass");
    //qurlq.setQuery(qurlq.query() + "?apikey=GxDMrBTSZJXDiJnTkJPCvVLZyvXQs83ghVCLAklLuH0NFleKNeEbdBPBGZvxigqZ");


    qurl.setScheme("https");
    qurl.setHost("orion.apiseeds.com");
    //qurl.("/ws/1.1");
    //qurl.setPath("/api/music/lyric/");
    qurl.setPath("/api/music/lyric/""blondie/heart%20of%20glass");


    qurl.setQuery(qurlq);



    //qurl = qurl.toEncoded();

    req.setUrl(qurl);
    //req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    // bad:
    // https://orion.apiseeds.com/api/music/lyric/?=blondie/heart of glass&apikey=GxDMrBTSZJXDiJnTkJPCvVLZyvXQs83ghVCLAklLuH0NFleKNeEbdBPBGZvxigqZ
    // https://orion.apiseeds.com/api/music/lyric/blondie/heart of glass?apikey=GxDMrBTSZJXDiJnTkJPCvVLZyvXQs83ghVCLAklLuH0NFleKNeEbdBPBGZvxigqZ

    connect(&mgr,SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinishedLyric(QNetworkReply*)));

    qDebug() <<"qurl: "<< qurl;

    QNetworkReply *reply = mgr.get(req);

    /// since QNetworkAccessManager does not offer synchronous HTTP requests, we use an event loop to emulate it:
    QEventLoop eventLoop;
    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {



     // QString strReply = (QString)reply->readAll();
     // QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
     // QJsonObject jsonObj = jsonResponse.object();

     // qDebug() << "name: " << jsonObj["name"].toString();
     // qDebug() << "longitude: " << jsonObj.value(QString("coord")).toObject()["lon"].toDouble();

        qDebug() << "Reply: " << reply;
        qDebug() << "Reply: " << reply->readAll();
        cout<< "Reply: " << reply->readAll().toStdString();
        delete reply;

    }else{
        qDebug() << "Failure" <<reply->errorString();
        delete reply;
    }

    /// to avoid a memory leak on each click (hint from stackoverflow):
    connect(&mgr,&QNetworkAccessManager::finished, &mgr, &QNetworkAccessManager::deleteLater);

}

void MainLRCWindow::replyFinishedLyric(QNetworkReply *reply)
{
    qDebug() << "ReplySlot: " << reply->readAll();

    QString strReply = reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
    QJsonObject jsonObj = jsonResponse.object();

    //QVariantMap mainMap = jsonObj.toVariantMap();

    qDebug() << "END: " <<endl;

    foreach(const QString& key, jsonObj.keys()) {
            QJsonValue value = jsonObj.value(key);
            qDebug() << "Key = " << key << ", Value = " << value.toString();
    }

    //qDebug() << "result: " << jsonObj["result"].toString();


}
