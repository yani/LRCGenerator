/*--------------------------------------------------------------------
 * Copyright 2018-2019 Yani M. All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * 07/2018, Mexico City, MEX
 * 07/2019, El Paso, TX, USA
 *--------------------------------------------------------------------
*/

/* @file main.cpp
 * @brief Contains the method and variables definition of the LRC Generator application.
 *
 * This is a LRC Generator application developed in Qt intended to add time labels to a .txt file
 * which contains a lyric song. This will be improved and released gradually.
 *
 * @author YAN
*/

#include "mainlrcwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainLRCWindow w;
    w.show();

    return a.exec();
}
